
## 0.5.4 [10-15-2024]

* Changes made at 2024.10.14_20:45PM

See merge request itentialopensource/adapters/adapter-watchguard_firebox_mgmt!14

---

## 0.5.3 [09-19-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-watchguard_firebox_mgmt!12

---

## 0.5.2 [08-14-2024]

* Changes made at 2024.08.14_19:03PM

See merge request itentialopensource/adapters/adapter-watchguard_firebox_mgmt!11

---

## 0.5.1 [08-07-2024]

* Changes made at 2024.08.06_20:14PM

See merge request itentialopensource/adapters/adapter-watchguard_firebox_mgmt!10

---

## 0.5.0 [05-16-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/security/adapter-watchguard_firebox_mgmt!9

---

## 0.4.4 [03-27-2024]

* Changes made at 2024.03.27_13:47PM

See merge request itentialopensource/adapters/security/adapter-watchguard_firebox_mgmt!8

---

## 0.4.3 [03-11-2024]

* Changes made at 2024.03.11_16:17PM

See merge request itentialopensource/adapters/security/adapter-watchguard_firebox_mgmt!7

---

## 0.4.2 [02-27-2024]

* Changes made at 2024.02.27_11:52AM

See merge request itentialopensource/adapters/security/adapter-watchguard_firebox_mgmt!6

---

## 0.4.1 [01-09-2024]

* fix links in metadata

See merge request itentialopensource/adapters/security/adapter-watchguard_firebox_mgmt!5

---

## 0.4.0 [01-05-2024]

* Minor/2023 auto migration

See merge request itentialopensource/adapters/security/adapter-watchguard_firebox_mgmt!4

---

## 0.3.0 [01-04-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-watchguard_firebox_mgmt!3

---

## 0.2.0 [05-29-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/security/adapter-watchguard_firebox_mgmt!2

---

## 0.1.2 [03-16-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/security/adapter-watchguard_firebox_mgmt!1

---

## 0.1.1 [10-12-2020]

- Initial Commit

See commit 76ffb20

---
