# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Watchguard_firebox_mgmt System. The API that was used to build the adapter for Watchguard_firebox_mgmt is usually available in the report directory of this adapter. The adapter utilizes the Watchguard_firebox_mgmt API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Watchguard Firebox Management adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Watchguard Firebox Management in order to manage WatchGuard Fireboxes. With this adapter you have the ability to perform operations on items such as:

- Deployments
- Health

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
