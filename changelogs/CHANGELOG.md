## 0.2.0 [05-29-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/security/adapter-watchguard_firebox_mgmt!2

---