/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-watchguard_firebox_mgmt',
      type: 'WatchguardFireboxMgmt',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const WatchguardFireboxMgmt = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Watchguard_firebox_mgmt Adapter Test', () => {
  describe('WatchguardFireboxMgmt Class Tests', () => {
    const a = new WatchguardFireboxMgmt(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-watchguard_firebox_mgmt-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-watchguard_firebox_mgmt-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const exceptionsAccountid = 'fakedata';
    describe('#retrievesexceptionssavedinthespecifiedWatchGuardCloudaccount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrievesexceptionssavedinthespecifiedWatchGuardCloudaccount(exceptionsAccountid, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Exceptions', 'retrievesexceptionssavedinthespecifiedWatchGuardCloudaccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const exceptionsBlockedSitesAccountid = 'fakedata';
    const exceptionsBlockedSitesCreatesanewblockedsitesexceptionThevalueoftheactionparameterspecifieswhethertheFireboxallowsorblockstrafficforthesiteBodyParam = {
      address: {}
    };
    describe('#createsanewblockedsitesexceptionThevalueoftheactionparameterspecifieswhethertheFireboxallowsorblockstrafficforthesite - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createsanewblockedsitesexceptionThevalueoftheactionparameterspecifieswhethertheFireboxallowsorblockstrafficforthesite(exceptionsBlockedSitesAccountid, exceptionsBlockedSitesCreatesanewblockedsitesexceptionThevalueoftheactionparameterspecifieswhethertheFireboxallowsorblockstrafficforthesiteBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.object);
                assert.equal(1, data.response.version);
                assert.equal('WGC-1-123abc456', data.response.account);
                assert.equal(1579907960, data.response.created);
                assert.equal(12345, data.response.device);
                assert.equal('string', data.response.author);
                assert.equal('Block example.com', data.response.description);
                assert.equal('object', typeof data.response.address);
                assert.equal('block', data.response.action);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsBlockedSites', 'createsanewblockedsitesexceptionThevalueoftheactionparameterspecifieswhethertheFireboxallowsorblockstrafficforthesite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrievesblockedsitesexceptionssavedinthespecifiedWatchGuardCloudaccount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrievesblockedsitesexceptionssavedinthespecifiedWatchGuardCloudaccount(exceptionsBlockedSitesAccountid, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsBlockedSites', 'retrievesblockedsitesexceptionssavedinthespecifiedWatchGuardCloudaccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const exceptionsBlockedSitesObjectid = 'fakedata';
    const exceptionsBlockedSitesUpdatesthespecifiedblockedsitesexceptionBodyParam = {
      version: 1,
      address: {}
    };
    describe('#updatesthespecifiedblockedsitesexception - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatesthespecifiedblockedsitesexception(exceptionsBlockedSitesAccountid, exceptionsBlockedSitesObjectid, exceptionsBlockedSitesUpdatesthespecifiedblockedsitesexceptionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsBlockedSites', 'updatesthespecifiedblockedsitesexception', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrievesthespecifiedblockedsitesexception - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrievesthespecifiedblockedsitesexception(exceptionsBlockedSitesAccountid, exceptionsBlockedSitesObjectid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.object);
                assert.equal(1, data.response.version);
                assert.equal('WGC-1-123abc456', data.response.account);
                assert.equal(1579907960, data.response.created);
                assert.equal(12345, data.response.device);
                assert.equal('string', data.response.author);
                assert.equal('Block example.com', data.response.description);
                assert.equal('object', typeof data.response.address);
                assert.equal('allow', data.response.action);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsBlockedSites', 'retrievesthespecifiedblockedsitesexception', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const exceptionsBotnetAccountid = 'fakedata';
    const exceptionsBotnetCreatesanewbotnetsiteexceptionBodyParam = {
      address: {}
    };
    describe('#createsanewbotnetsiteexception - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createsanewbotnetsiteexception(exceptionsBotnetAccountid, exceptionsBotnetCreatesanewbotnetsiteexceptionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.object);
                assert.equal(1, data.response.version);
                assert.equal('WGC-1-123abc456', data.response.account);
                assert.equal(1579907960, data.response.created);
                assert.equal(12345, data.response.device);
                assert.equal('string', data.response.author);
                assert.equal('Allow example.com', data.response.description);
                assert.equal('object', typeof data.response.address);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsBotnet', 'createsanewbotnetsiteexception', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrievesbotnetsiteexceptionssavedinthespecifiedWatchGuardCloudaccount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrievesbotnetsiteexceptionssavedinthespecifiedWatchGuardCloudaccount(exceptionsBotnetAccountid, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsBotnet', 'retrievesbotnetsiteexceptionssavedinthespecifiedWatchGuardCloudaccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const exceptionsBotnetObjectid = 'fakedata';
    const exceptionsBotnetUpdatesthespecifiedbotnetsiteexceptionBodyParam = {
      version: 1,
      address: {}
    };
    describe('#updatesthespecifiedbotnetsiteexception - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatesthespecifiedbotnetsiteexception(exceptionsBotnetAccountid, exceptionsBotnetObjectid, exceptionsBotnetUpdatesthespecifiedbotnetsiteexceptionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsBotnet', 'updatesthespecifiedbotnetsiteexception', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrievesthespecifiedbotnetsiteexception - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrievesthespecifiedbotnetsiteexception(exceptionsBotnetAccountid, exceptionsBotnetObjectid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.object);
                assert.equal(1, data.response.version);
                assert.equal('WGC-1-123abc456', data.response.account);
                assert.equal(1579907960, data.response.created);
                assert.equal(12345, data.response.device);
                assert.equal('string', data.response.author);
                assert.equal('Allow example.com', data.response.description);
                assert.equal('object', typeof data.response.address);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsBotnet', 'retrievesthespecifiedbotnetsiteexception', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const exceptionsFileAccountid = 'fakedata';
    const exceptionsFileCreatesanewfileexceptionBodyParam = {
      action: 'allow',
      md5: 'd2ffd57b48ee2ac34a4797ea44571a05'
    };
    describe('#createsanewfileexception - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createsanewfileexception(exceptionsFileAccountid, exceptionsFileCreatesanewfileexceptionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.object);
                assert.equal(1, data.response.version);
                assert.equal('WGC-1-123abc456', data.response.account);
                assert.equal(1579907960, data.response.created);
                assert.equal(12345, data.response.device);
                assert.equal('string', data.response.author);
                assert.equal('drop', data.response.action);
                assert.equal('Allow Sales Presentation.', data.response.description);
                assert.equal('d2ffd57b48ee2ac34a4797ea44571a05', data.response.md5);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsFile', 'createsanewfileexception', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrievesfileexceptionssavedinthespecifiedWatchGuardCloudaccount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrievesfileexceptionssavedinthespecifiedWatchGuardCloudaccount(exceptionsFileAccountid, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsFile', 'retrievesfileexceptionssavedinthespecifiedWatchGuardCloudaccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const exceptionsFileObjectid = 'fakedata';
    const exceptionsFileUpdatesthespecifiedfileexceptionBodyParam = {
      version: 1,
      action: 'allow',
      md5: 'd2ffd57b48ee2ac34a4797ea44571a05'
    };
    describe('#updatesthespecifiedfileexception - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatesthespecifiedfileexception(exceptionsFileAccountid, exceptionsFileObjectid, exceptionsFileUpdatesthespecifiedfileexceptionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsFile', 'updatesthespecifiedfileexception', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrievesthespecifiedfileexception - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrievesthespecifiedfileexception(exceptionsFileAccountid, exceptionsFileObjectid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.object);
                assert.equal(1, data.response.version);
                assert.equal('WGC-1-123abc456', data.response.account);
                assert.equal(1579907960, data.response.created);
                assert.equal(12345, data.response.device);
                assert.equal('string', data.response.author);
                assert.equal('drop', data.response.action);
                assert.equal('Allow Sales Presentation.', data.response.description);
                assert.equal('d2ffd57b48ee2ac34a4797ea44571a05', data.response.md5);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsFile', 'retrievesthespecifiedfileexception', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const exceptionsGeolocationAccountid = 'fakedata';
    const exceptionsGeolocationCreatesanewGeolocationexceptionBodyParam = {
      address: {}
    };
    describe('#createsanewGeolocationexception - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createsanewGeolocationexception(exceptionsGeolocationAccountid, exceptionsGeolocationCreatesanewGeolocationexceptionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.object);
                assert.equal(1, data.response.version);
                assert.equal('WGC-1-123abc456', data.response.account);
                assert.equal(1579907960, data.response.created);
                assert.equal(12345, data.response.device);
                assert.equal('string', data.response.author);
                assert.equal('Allow example.com', data.response.description);
                assert.equal('object', typeof data.response.address);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsGeolocation', 'createsanewGeolocationexception', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrievesallGeolocationexceptionssavedinthespecifiedWatchGuardCloudaccount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrievesallGeolocationexceptionssavedinthespecifiedWatchGuardCloudaccount(exceptionsGeolocationAccountid, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsGeolocation', 'retrievesallGeolocationexceptionssavedinthespecifiedWatchGuardCloudaccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const exceptionsGeolocationObjectid = 'fakedata';
    const exceptionsGeolocationUpdatesthespecifiedGeolocationexceptionBodyParam = {
      version: 1,
      address: {}
    };
    describe('#updatesthespecifiedGeolocationexception - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatesthespecifiedGeolocationexception(exceptionsGeolocationAccountid, exceptionsGeolocationObjectid, exceptionsGeolocationUpdatesthespecifiedGeolocationexceptionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsGeolocation', 'updatesthespecifiedGeolocationexception', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrievesthespecifiedGeolocationexception - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrievesthespecifiedGeolocationexception(exceptionsGeolocationAccountid, exceptionsGeolocationObjectid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.object);
                assert.equal(1, data.response.version);
                assert.equal('WGC-1-123abc456', data.response.account);
                assert.equal(1579907960, data.response.created);
                assert.equal(12345, data.response.device);
                assert.equal('string', data.response.author);
                assert.equal('Allow example.com', data.response.description);
                assert.equal('object', typeof data.response.address);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsGeolocation', 'retrievesthespecifiedGeolocationexception', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const exceptionsIntrusionPreventionAccountid = 'fakedata';
    const exceptionsIntrusionPreventionCreatesanewIPSsignatureexceptionBodyParam = {
      action: 'drop',
      signature_id: '1055124'
    };
    describe('#createsanewIPSsignatureexception - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createsanewIPSsignatureexception(exceptionsIntrusionPreventionAccountid, exceptionsIntrusionPreventionCreatesanewIPSsignatureexceptionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.object);
                assert.equal(1, data.response.version);
                assert.equal('WGC-1-123abc456', data.response.account);
                assert.equal(1579907960, data.response.created);
                assert.equal(12345, data.response.device);
                assert.equal('string', data.response.author);
                assert.equal('allow', data.response.action);
                assert.equal(true, data.response.alarm);
                assert.equal('Drop signature 1055124', data.response.description);
                assert.equal('1055124', data.response.signature_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsIntrusionPrevention', 'createsanewIPSsignatureexception', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrievesallIPSsignatureexceptions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrievesallIPSsignatureexceptions(exceptionsIntrusionPreventionAccountid, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsIntrusionPrevention', 'retrievesallIPSsignatureexceptions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const exceptionsIntrusionPreventionObjectid = 'fakedata';
    const exceptionsIntrusionPreventionUpdatesthespecifiedIPSsignatureexceptionBodyParam = {
      version: 1,
      action: 'allow',
      signature_id: '1055124'
    };
    describe('#updatesthespecifiedIPSsignatureexception - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatesthespecifiedIPSsignatureexception(exceptionsIntrusionPreventionAccountid, exceptionsIntrusionPreventionObjectid, exceptionsIntrusionPreventionUpdatesthespecifiedIPSsignatureexceptionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsIntrusionPrevention', 'updatesthespecifiedIPSsignatureexception', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrievesthespecifiedIPSsignatureexception - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrievesthespecifiedIPSsignatureexception(exceptionsIntrusionPreventionAccountid, exceptionsIntrusionPreventionObjectid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.object);
                assert.equal(1, data.response.version);
                assert.equal('WGC-1-123abc456', data.response.account);
                assert.equal(1579907960, data.response.created);
                assert.equal(12345, data.response.device);
                assert.equal('string', data.response.author);
                assert.equal('block', data.response.action);
                assert.equal(true, data.response.alarm);
                assert.equal('Drop signature 1055124', data.response.description);
                assert.equal('1055124', data.response.signature_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsIntrusionPrevention', 'retrievesthespecifiedIPSsignatureexception', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const exceptionsWebBlockerAccountid = 'fakedata';
    const exceptionsWebBlockerCreatesanewWebBlockerexceptionBodyParam = {
      action: 'deny',
      name: 'Allow example.com',
      rule_type: 'pattern',
      rule: 'www.example.com/*, 1.1.1.1, or (www\\.)?example\\.[com|net]'
    };
    describe('#createsanewWebBlockerexception - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createsanewWebBlockerexception(exceptionsWebBlockerAccountid, exceptionsWebBlockerCreatesanewWebBlockerexceptionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.object);
                assert.equal(1, data.response.version);
                assert.equal('WGC-1-123abc456', data.response.account);
                assert.equal(1579907960, data.response.created);
                assert.equal(12345, data.response.device);
                assert.equal('string', data.response.author);
                assert.equal('allow', data.response.action);
                assert.equal(true, data.response.alarm);
                assert.equal('Allow example.com', data.response.name);
                assert.equal('regexp', data.response.rule_type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsWebBlocker', 'createsanewWebBlockerexception', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrievesallWebBlockerexceptions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrievesallWebBlockerexceptions(exceptionsWebBlockerAccountid, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsWebBlocker', 'retrievesallWebBlockerexceptions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const exceptionsWebBlockerObjectid = 'fakedata';
    const exceptionsWebBlockerUpdatesthespecifiedWebBlockerexceptionBodyParam = {
      version: 1,
      action: 'deny',
      name: 'Allow example.com',
      rule_type: 'regexp',
      rule: 'www.example.com/*, 1.1.1.1, or (www\\.)?example\\.[com|net]'
    };
    describe('#updatesthespecifiedWebBlockerexception - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatesthespecifiedWebBlockerexception(exceptionsWebBlockerAccountid, exceptionsWebBlockerObjectid, exceptionsWebBlockerUpdatesthespecifiedWebBlockerexceptionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsWebBlocker', 'updatesthespecifiedWebBlockerexception', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrievesthespecifiedWebBlockerexception - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrievesthespecifiedWebBlockerexception(exceptionsWebBlockerAccountid, exceptionsWebBlockerObjectid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.object);
                assert.equal(1, data.response.version);
                assert.equal('WGC-1-123abc456', data.response.account);
                assert.equal(1579907960, data.response.created);
                assert.equal(12345, data.response.device);
                assert.equal('string', data.response.author);
                assert.equal('allow', data.response.action);
                assert.equal(true, data.response.alarm);
                assert.equal('Allow example.com', data.response.name);
                assert.equal('pattern', data.response.rule_type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsWebBlocker', 'retrievesthespecifiedWebBlockerexception', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const globalExceptionsAccountid = 'fakedata';
    const globalExceptionsDisablestheabilitytomanageanddeployglobalexceptionswiththeFireboxManagementAPIforthespecifieddeviceSYoucanspecifywhethertoretainordeleteexceptionsonthedeviceSBodyParam = {
      devices: [
        4
      ]
    };
    describe('#disablestheabilitytomanageanddeployglobalexceptionswiththeFireboxManagementAPIforthespecifieddeviceSYoucanspecifywhethertoretainordeleteexceptionsonthedeviceS - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.disablestheabilitytomanageanddeployglobalexceptionswiththeFireboxManagementAPIforthespecifieddeviceSYoucanspecifywhethertoretainordeleteexceptionsonthedeviceS(globalExceptionsAccountid, globalExceptionsDisablestheabilitytomanageanddeployglobalexceptionswiththeFireboxManagementAPIforthespecifieddeviceSYoucanspecifywhethertoretainordeleteexceptionsonthedeviceSBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-watchguard_firebox_mgmt-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GlobalExceptions', 'disablestheabilitytomanageanddeployglobalexceptionswiththeFireboxManagementAPIforthespecifieddeviceSYoucanspecifywhethertoretainordeleteexceptionsonthedeviceS', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const globalExceptionsEnablestheabilitytomanageanddeployexceptionswiththeFireboxManagementAPIforthespecifieddeviceSBodyParam = {
      devices: [
        3
      ]
    };
    describe('#enablestheabilitytomanageanddeployexceptionswiththeFireboxManagementAPIforthespecifieddeviceS - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.enablestheabilitytomanageanddeployexceptionswiththeFireboxManagementAPIforthespecifieddeviceS(globalExceptionsAccountid, globalExceptionsEnablestheabilitytomanageanddeployexceptionswiththeFireboxManagementAPIforthespecifieddeviceSBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-watchguard_firebox_mgmt-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GlobalExceptions', 'enablestheabilitytomanageanddeployexceptionswiththeFireboxManagementAPIforthespecifieddeviceS', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deploymentsAccountid = 'fakedata';
    const deploymentsDeploysexceptionssavedinWatchGuardCloudtothespecifieddevicesBodyParam = {
      devices: [
        7
      ],
      start_date: '2020-04-19T21:23:46Z'
    };
    describe('#deploysexceptionssavedinWatchGuardCloudtothespecifieddevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deploysexceptionssavedinWatchGuardCloudtothespecifieddevices(deploymentsAccountid, deploymentsDeploysexceptionssavedinWatchGuardCloudtothespecifieddevicesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deployments', 'deploysexceptionssavedinWatchGuardCloudtothespecifieddevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deploymentsTxType = 'fakedata';
    describe('#retrievesalistoftransactionrecordsWhichincludedetailsofindividualdeployments - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrievesalistoftransactionrecordsWhichincludedetailsofindividualdeployments(deploymentsAccountid, null, deploymentsTxType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deployments', 'retrievesalistoftransactionrecordsWhichincludedetailsofindividualdeployments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deploymentsObjectid = 'fakedata';
    const deploymentsUpdatesthespecifiedtransactionrecordBodyParam = {
      start_date: '2020-04-19T21:23:46Z'
    };
    describe('#updatesthespecifiedtransactionrecord - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatesthespecifiedtransactionrecord(deploymentsAccountid, deploymentsObjectid, deploymentsUpdatesthespecifiedtransactionrecordBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deployments', 'updatesthespecifiedtransactionrecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrievesthespecifiedtransactionrecordWhichincludesdetailsofaspecificdeployment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrievesthespecifiedtransactionrecordWhichincludesdetailsofaspecificdeployment(deploymentsAccountid, deploymentsObjectid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deployments', 'retrievesthespecifiedtransactionrecordWhichincludesdetailsofaspecificdeployment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#healthGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.healthGET((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.healthy);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Health', 'healthGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletesthespecifiedblockedsitesexception - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deletesthespecifiedblockedsitesexception(exceptionsBlockedSitesAccountid, exceptionsBlockedSitesObjectid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsBlockedSites', 'deletesthespecifiedblockedsitesexception', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletesthespecifiedbotnetsiteexception - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deletesthespecifiedbotnetsiteexception(exceptionsBotnetAccountid, exceptionsBotnetObjectid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsBotnet', 'deletesthespecifiedbotnetsiteexception', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletesthespecifiedfileexception - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deletesthespecifiedfileexception(exceptionsFileAccountid, exceptionsFileObjectid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsFile', 'deletesthespecifiedfileexception', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletesthespecifiedGeolocationexception - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deletesthespecifiedGeolocationexception(exceptionsGeolocationAccountid, exceptionsGeolocationObjectid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsGeolocation', 'deletesthespecifiedGeolocationexception', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletesthespecifiedIPSsignatureexception - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deletesthespecifiedIPSsignatureexception(exceptionsIntrusionPreventionAccountid, exceptionsIntrusionPreventionObjectid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsIntrusionPrevention', 'deletesthespecifiedIPSsignatureexception', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletesthespecifiedWebBlockerexception - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deletesthespecifiedWebBlockerexception(exceptionsWebBlockerAccountid, exceptionsWebBlockerObjectid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExceptionsWebBlocker', 'deletesthespecifiedWebBlockerexception', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletesthespecifiedtransactionrecord - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deletesthespecifiedtransactionrecord(deploymentsAccountid, deploymentsObjectid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deployments', 'deletesthespecifiedtransactionrecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
